console.log('page load - entered main.js');

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo;


function getFormInfo(){
    console.log('entered getFormInfo function')
    console.log();
    makeNetworkCallToIPApi();
}

function makeNetworkCallToIPApi() {
    console.log('entered makeNetworkCallToIPApi');
    var url = 'https://api.ipify.org/?format=json'
    console.log(url);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        console.log('network response recieved' + xhr.responseText);
        updateWithIP(xhr.responseText);
    }
    
    xhr.onerror = function(e){
        console.log('error occured');
        console.error(xhr.statusText);
    }
    
    xhr.send(null);
}

function updateWithIP(response_text){
    // extract info from response
    var json = JSON.parse(response_text);
    var ip = json['ip']
    var display_text = 'Your computers ip address is: ' + ip
    // update label with info
    var label_item = document.createElement("p");
    label_item.setAttribute("id", "dyanmic-ip");
    var item_text = document.createTextNode(display_text);
    label_item.appendChild(item_text);
    console.log(label_item)

    var response_div = document.getElementById('response-div');
    response_div.appendChild(label_item);

    makeNetworkCallToRegionApi(ip);
}

function makeNetworkCallToRegionApi(ip_address){

    // make the api call and get results
    var url = 'https://ipinfo.io/' + ip_address + '/geo'
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true); 
    
    xhr.onload = function(e){
        console.log('network response recieved' + xhr.responseText);
        updateWithRegion(xhr.responseText);
    }
    
    xhr.onerror = function(e){
        console.log('error occured');
        console.error(xhr.statusText);
    }
    
    xhr.send(null);

}

function updateWithRegion(response_text){
    // placeholder
    var json = JSON.parse(response_text);
    var city = json['city']
    var display_text = 'This means you are located in: ' + city
    
    // update label with info
    var label_item = document.createElement("p");
    label_item.setAttribute("id", "dynamic-city");
    var item_text = document.createTextNode(display_text);
    label_item.appendChild(item_text);
    console.log(label_item)

    var response_div = document.getElementById('response-div');
    response_div.appendChild(label_item);
}